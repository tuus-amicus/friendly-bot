package ru.bot.friendlybot.service

import org.springframework.stereotype.Service
import ru.bot.friendlybot.client.AdviceClient
import ru.bot.friendlybot.data.Advice
import ru.bot.friendlybot.data.AdviceHistory
import ru.bot.friendlybot.repository.AdviceHistoryRepository
import java.time.LocalDate

@Service
class AdviceService(private val adviceHistoryRepository: AdviceHistoryRepository,
                    private val adviceClient: AdviceClient) {
    fun getAdvice(userId: Long): Advice {
        val adviceHistory = adviceHistoryRepository.findByUserIdAndDate(userId, LocalDate.now())

        val advice = adviceHistory?.let {
            return Advice(it.adviceId, it.text)
        } ?: adviceClient.random()

        val history = AdviceHistory(adviceId = advice.id, userId = userId, text = advice.text, creationDate = LocalDate.now())

        adviceHistoryRepository.save(history)
        return advice
    }
}