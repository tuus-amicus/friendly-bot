package ru.bot.friendlybot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FriendlyBotApplication

fun main(args: Array<String>) {
    runApplication<FriendlyBotApplication>(*args)
}