package ru.bot.friendlybot

import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import ru.bot.friendlybot.config.BotProperties
import ru.bot.friendlybot.handlers.CommandHandler

@Component
class TelegramBot(private val handlers: List<CommandHandler>,
                  private val botProperties: BotProperties) : ApplicationRunner {

    private val bot = bot {
        token = botProperties.token
        dispatch {
            handlers.forEach {
                command(it.command) { bot, update ->
                    it.command(update.message!!, bot)
                }
            }
        }
    }

    override fun run(args: ApplicationArguments?) {
        bot.startPolling()
    }
}