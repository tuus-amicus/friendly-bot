package ru.bot.friendlybot.data

data class Advice(
    val id: Long,
    val text: String
)