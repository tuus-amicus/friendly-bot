package ru.bot.friendlybot.data

import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate

data class AdviceHistory(
    val adviceId: Long,
    val userId: Long,
    val text: String,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val creationDate: LocalDate
)