package ru.bot.friendlybot.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@ConfigurationProperties(prefix = "telegram.bot")
@ConstructorBinding
@Validated
class BotProperties(@field:NotBlank val token: String)