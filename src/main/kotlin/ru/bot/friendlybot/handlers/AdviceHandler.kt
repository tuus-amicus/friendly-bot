package ru.bot.friendlybot.handlers

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.Message
import org.springframework.stereotype.Component
import ru.bot.friendlybot.service.AdviceService

@Component
class AdviceHandler(private val adviceService: AdviceService) : CommandHandler {
    override val command = "advice"

    override fun command(message: Message, bot: Bot) = run {
        val advice = adviceService.getAdvice(message.chat.id)
        bot.sendMessage(chatId = message.chat.id, text = advice.text)
    }
}