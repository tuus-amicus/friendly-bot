package ru.bot.friendlybot.handlers

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.Message
import com.github.kotlintelegrambot.network.Response

/**
 * Describe interface for command handlers
 */
interface CommandHandler {
    val command: String

    fun command(message: Message, bot: Bot): Pair<retrofit2.Response<Response<Message>?>?, Exception?>
}