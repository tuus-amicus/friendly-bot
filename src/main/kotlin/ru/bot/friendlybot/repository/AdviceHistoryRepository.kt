package ru.bot.friendlybot.repository

import ru.bot.friendlybot.data.AdviceHistory
import java.time.LocalDate

interface AdviceHistoryRepository {
    fun findByUserIdAndDate(userId: Long, adviceDate: LocalDate): AdviceHistory?
    fun save(history: AdviceHistory)
    fun deleteAll()
}