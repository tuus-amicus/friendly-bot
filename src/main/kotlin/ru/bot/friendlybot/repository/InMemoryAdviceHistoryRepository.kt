package ru.bot.friendlybot.repository

import org.springframework.stereotype.Repository
import ru.bot.friendlybot.data.AdviceHistory
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.concurrent.ConcurrentHashMap


@Repository
class InMemoryAdviceHistoryRepository : AdviceHistoryRepository {
    private val memory = ConcurrentHashMap<Long, AdviceHistory>()
    private val dateFormatter = DateTimeFormatter.ISO_DATE

    override fun findByUserIdAndDate(userId: Long, adviceDate: LocalDate): AdviceHistory? =
        memory[userId]?.takeIf {
            dateFormatter.format(it.creationDate) == dateFormatter.format(adviceDate)
        }

    override fun save(history: AdviceHistory) {
        memory[history.userId] = history
    }

    override fun deleteAll() {
        memory.clear()
    }
}