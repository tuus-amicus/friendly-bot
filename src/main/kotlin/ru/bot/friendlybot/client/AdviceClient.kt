package ru.bot.friendlybot.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import ru.bot.friendlybot.data.Advice

@FeignClient(name = "advice", url = "http://fucking-great-advice.ru/api")
interface AdviceClient {
    @GetMapping("/random")
    fun random(): Advice
}