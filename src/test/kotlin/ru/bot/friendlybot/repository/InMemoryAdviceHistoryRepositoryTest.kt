package ru.bot.friendlybot.repository

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import ru.bot.friendlybot.data.AdviceHistory
import java.time.LocalDate

@SpringBootTest(classes = [InMemoryAdviceHistoryRepository::class])
internal class InMemoryAdviceHistoryRepositoryTest {
    @Autowired
    private lateinit var repository: AdviceHistoryRepository

    @Test
    fun `find by userId and date works`() {
        val history = history()
        repository.findByUserIdAndDate(userId = history.userId, adviceDate = history.creationDate) shouldBe null

        repository.save(history = history)

        repository.findByUserIdAndDate(history.userId, history.creationDate) shouldBe history

    }

    private fun history() = AdviceHistory(adviceId = 1L, userId = 12L, text = "test", creationDate =  LocalDate.now())
}