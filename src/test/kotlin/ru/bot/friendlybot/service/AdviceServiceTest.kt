package ru.bot.friendlybot.service

import com.ninjasquad.springmockk.MockkBean
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import ru.bot.friendlybot.client.AdviceClient
import ru.bot.friendlybot.data.Advice
import ru.bot.friendlybot.data.AdviceHistory
import ru.bot.friendlybot.repository.AdviceHistoryRepository
import java.time.LocalDate

@SpringBootTest(classes = [AdviceService::class])
internal class AdviceServiceTest {
    @Autowired
    private lateinit var adviceService: AdviceService

    @MockkBean
    private lateinit var adviceClient: AdviceClient

    @MockkBean
    private lateinit var adviceHistoryRepository: AdviceHistoryRepository

    private val advice = Advice(1L, "hello, boy")
    private val userId = 13L
    private val adviceFromRepo =
        AdviceHistory(5L, userId = userId, text = "hello, guys", creationDate = LocalDate.now())

    @BeforeEach
    fun setup() {
        clearAllMocks()
    }

    @Test
    fun `should give advice from client`() {
        every { adviceClient.random() } returns advice
        every { adviceHistoryRepository.findByUserIdAndDate(userId, any()) } returns null
        every { adviceHistoryRepository.save(any()) } just Runs

        adviceService.getAdvice(userId) shouldBe advice
    }

    @Test
    fun `should give advice from repository`() {
        every { adviceHistoryRepository.findByUserIdAndDate(userId, any()) } returns adviceFromRepo

        adviceService.getAdvice(userId) shouldBe Advice(adviceFromRepo.adviceId, adviceFromRepo.text)

        verify(exactly = 0) {
            adviceClient.random()
        }
    }
}